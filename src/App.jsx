import "./App.css";
import React, { useState } from "react";
import "react-toastify/dist/ReactToastify.css";
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect,
} from "react-router-dom";
import { UserContext } from "./resources/UserContext";
import NavigationComponent from "./components/UI/NavigationComponent";
import QuestionsPage from "./components/pages/QuestionsPage";
import QuestionPage from "./components/pages/QuestionPage";
import AccessDeniedPage from "./components/pages/AccessDeniedPage";
import LoginPage from "./components/pages/LoginPage";
import RegisterPage from "./components/pages/RegisterPage";
import TagsComponent from "./components/pages/TagsPage";

const App = () => {
	const [user, setUser] = useState(null);

	return (
		<UserContext.Provider value={{ user, setUser }}>
			<Router>
				{user && !user.banned && <NavigationComponent />}
				<Switch>
					<Route path="/login" exact component={LoginPage} />
					<Route
						exact
						path="/register"
						component={RegisterPage}
					/>
					{user && !user.banned && (
						<>
							<Route
								exact
								path="/question/:questionId"
								component={QuestionPage}
							/>
							<Route
								exact
								path="/questions"
								component={QuestionsPage}
							/>
							<Route
								exact
								path="/tags"
								component={TagsComponent}
							/>
						</>
					)}
					<Route
						path="/access-denied"
						exact
						component={AccessDeniedPage}
					/>
					<Route
						render={() =>
							!user || user.banned ? (
								<Redirect to="/access-denied" />
							) : (
								<Redirect to="/login" />
							)
						}
					/>
				</Switch>
			</Router>
		</UserContext.Provider>
	);
};

export default App;
