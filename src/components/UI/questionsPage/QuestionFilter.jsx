import React, { useState } from "react";
import { Checkbox, Input } from "semantic-ui-react";

const QuestionFilter = ({ initialQuestions, changeQuestions }) => {
	const [searchByQuestion, setSearchByQuestion] = useState(true);

	const onFilterChange = (event, data) => {
		if (data.value.trim().length === 0) {
			changeQuestions(initialQuestions);
			return;
		}

		if (searchByQuestion) {
			changeQuestions(
				initialQuestions.filter((question) =>
					question.title
						.toLowerCase()
						.includes(data.value.toLowerCase())
				)
			);
		} else {
			changeQuestions(
				initialQuestions.filter(
					(question) =>
						question.tagsList.find((t) =>
							t.name
								.toLowerCase()
								.includes(data.value.toLowerCase())
						) !== undefined
				)
			);
		}
	};

	return (
		<>
			<Input
				icon="search"
				placeholder="Search..."
				onChange={onFilterChange}
			/>
			<Checkbox
				toggle
				label="By Tag"
				onChange={() => setSearchByQuestion(!searchByQuestion)}
			/>
		</>
	);
};

export default QuestionFilter;
