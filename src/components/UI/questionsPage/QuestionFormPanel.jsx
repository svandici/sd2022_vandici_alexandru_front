import React, { useState, useEffect } from "react";
import { toast } from "react-toastify";
import { Modal, Form, Button } from "semantic-ui-react";
import UserService from "../../../services/UserService";

const emptyQuestion = {
	title: "",
	text: "",
	date: new Date(),
	upvotes: 0,
	author: null,
	tagsList: [],
};

const QuestionFormPanel = ({ onClose, open, onSave, userID, tags }) => {
	const [question, setQuestion] = useState(emptyQuestion);
	const [options, setOptions] = useState([]);

	useEffect(() => {
		UserService.getUser(userID)
			.then((response) => {
				setQuestion({ ...question, author: response.data });
			})
			.catch((err) => {
				toast.error("Something went wrong while getting the user");
			});
	// eslint-disable-next-line
	}, [userID]);

	useEffect(() => {
		setOptions(
			tags.map((tag) => {
				return { key: tag.id, text: tag.name, value: tag };
			})
		);
	}, [tags]);

	const onTitleChange = (event) => {
		setQuestion({ ...question, title: event.target.value });
	};

	const onTextChange = (event) => {
		setQuestion({ ...question, text: event.target.value });
	};

	const onSelectChange = (event, data) => {
		setQuestion((prevQuestion) => {
			return { ...prevQuestion, tagsList: data.value };
		});
	};

	const onSaveQuestion = async () => {
		if(question.author === null) {
			toast.error("You must login again");
			return;
		}
		await onSave(question);
		onClose();
		setQuestion(emptyQuestion);
	};

	const onCloseModal = () => {
		setQuestion(emptyQuestion);
		onClose();
	};

	return (
		<Modal onClose={onCloseModal} open={open}>
			<Modal.Header>Ask your question</Modal.Header>
			<Modal.Content>
				<Form>
					<Form.Group widths="equal">
						<Form.Input
							label="Title"
							onChange={onTitleChange}
							value={question.title}
						/>
					</Form.Group>
					<Form.TextArea
						label="Text"
						style={{ minHeight: 400 }}
						onChange={onTextChange}
						value={question.text}
					/>
					<Form.Select
						fluid
						label="Tags"
						options={options}
						multiple
						search
						placeholder="Add tags to describe what your question is about"
						onChange={onSelectChange}
					/>
				</Form>
			</Modal.Content>
			<Modal.Actions>
				<Button
					content="Save"
					labelPosition="right"
					icon="checkmark"
					onClick={onSaveQuestion}
					color="orange"
					disabled={
						question.title.trim().length === 0 ||
						question.text.trim().length === 0
					}
				/>
				<Button onClick={onCloseModal} content="Close" secondary />
			</Modal.Actions>
		</Modal>
	);
};

export default QuestionFormPanel;
