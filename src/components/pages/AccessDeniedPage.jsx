import React, { useContext} from "react";
import { useHistory } from "react-router";
import { Grid, Card, Icon, Button } from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";

const AccessDeniedPage = () => {
	let history = useHistory();
	const { user } = useContext(UserContext);

	const redirectHandler = () => {
		history.push("/login");
	};

	return (
		<Grid centered verticalAlign="middle" className="access-denied">
			<Card>
				<Card.Content>
					<Icon name="dont" size="huge" color="red" />
					<Card.Description>
						{user && user.banned ? "You are banned" : "You must be logged in in order to access the site"}
					</Card.Description>
				</Card.Content>
				<Card.Content>
					<Button onClick={redirectHandler} color="orange">
						Sign-in
					</Button>
				</Card.Content>
			</Card>
		</Grid>
	);
};

export default AccessDeniedPage;
