import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";
import {
	Grid,
	Header,
	Form,
	Segment,
	Button,
	Message,
} from "semantic-ui-react";
import { UserContext } from "../../resources/UserContext";
import UserService from "../../services/UserService";

const LoginPage = () => {
	const [loginRequest, setLoginRequest] = useState({
		username: "",
		password: "",
	});
	let history = useHistory();
	const { setUser } = useContext(UserContext);

	const onUsernameChange = (event) => {
		setLoginRequest({ ...loginRequest, username: event.target.value });
	};

	const onPasswordChange = (event) => {
		setLoginRequest({ ...loginRequest, password: event.target.value });
	};

	const onLoginHandler = () => {
		UserService.login(loginRequest)
			.then((response) => {
				setUser(response.data);
				
				history.push("/questions");
			})
			.catch((err) => {
				toast.error(err && err.response ? err.response.data : err);
			});
	};

	return (
		<Grid
			textAlign="center"
			style={{ height: "80vh" }}
			verticalAlign="middle"
		>
			<Grid.Column style={{ maxWidth: 450 }}>
				<Header as="h2" color="orange" textAlign="center">
					Login
				</Header>
				<Form size="large">
					<Segment stacked>
						<Form.Input
							fluid
							icon="user"
							iconPosition="left"
							placeholder="Username"
							onChange={onUsernameChange}
						/>
						<Form.Input
							fluid
							icon="lock"
							iconPosition="left"
							placeholder="Password"
							type="password"
							onChange={onPasswordChange}
						/>

						<Button
							color="orange"
							fluid
							size="large"
							onClick={onLoginHandler}
							disabled={
								loginRequest.username.trim().length === 0 ||
								loginRequest.password.trim().length === 0
							}
						>
							Login
						</Button>
					</Segment>
				</Form>
				<Message>
					Don't have an account? <Link to="/register">Register</Link>
				</Message>
				<ToastContainer />
			</Grid.Column>
		</Grid>
	);
};

export default LoginPage;
