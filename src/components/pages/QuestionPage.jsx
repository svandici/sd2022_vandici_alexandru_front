import React, { useState, useEffect, useContext } from "react";
import CommentsSection from "../UI/questionEditPage/CommentsSection";
import { useLocation } from "react-router-dom";
import {
	Button,
	Dimmer,
	Grid,
	Header,
	Icon,
	Label,
	Loader,
	TextArea,
	Form,
	Modal,
} from "semantic-ui-react";
import AnswerService from "../../services/AnswerService";
import QuestionService from "../../services/QuestionService";
import { useHistory } from "react-router";
import { UserContext } from "../../resources/UserContext";
import { toast, ToastContainer } from "react-toastify";

const sortByVotes = (list) => {
	return list.sort((a, b) => {
		return b.upvotes - a.upvotes;
	});
};

const QuestionPage = () => {
	const { state } = useLocation();
	const [question, setQuestion] = useState(state.question);
	const [editQuestion, setEditQuestion] = useState(null);
	const [answers, setAnswers] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [confirmMsg, setConfirmMsg] = useState("");

	const { user } = useContext(UserContext);

	let history = useHistory();

	useEffect(() => {
		setIsLoading(true);
		AnswerService.getAnswersByQuestion(question.id)
			.then((rAnswers) => {
				setAnswers(rAnswers.data);
			})
			.catch((error) => {
				toast.error("Something went wrong getting the data");
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, [question.id, question.userID]);

	const onAnswerSave = (answer) => {
		setIsLoading(true);

		AnswerService.saveAnswer(answer)
			.then((response) => {
				setAnswers([...answers, response.data]);
			})
			.catch((error) => {
				toast.error("Something went wrong when saving the answer");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onAnswerUpdate = (answer) => {
		setIsLoading(true);

		AnswerService.updateAnswer(answer)
			.then((response) => {
				setAnswers((prevAnswers) => {
					prevAnswers.find(
						(prevAnswer) => prevAnswer.id === answer.id
					).text = response.data.text;

					prevAnswers.find(
						(prevAnswer) => prevAnswer.id === answer.id
					).upvotes = response.data.upvotes;
					return prevAnswers;
				});
			})
			.catch((error) => {
				toast.error("Something went wrong when updating the answer");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onAnswerDelete = (answerID) => {
		setIsLoading(true);

		AnswerService.deleteAnswer(answerID)
			.then(() => {
				setAnswers((prevAnswers) =>
					prevAnswers.filter((answer) => answer.id !== answerID)
				);
			})
			.catch((error) => {
				toast.error("Something went wrong when deleting the answer.");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onQuestionUpdate = (questionToUpdate) => {
		if (user.id === question.author.id) {
			toast.warning("You can not vote your own question");
			return;
		}

		QuestionService.updateQuestion(questionToUpdate)
			.then((response) => {
				setQuestion(response.data);
			})
			.catch((error) => {
				toast.error("Something went wrong updating the question");
			});
	};

	const onEditClicked = () => {
		if (!user.admin && user.id !== question.author.id) {
			toast.warning("You can not edit somebody's else question");
			return;
		}

		if (!editQuestion) {
			setEditQuestion(question);
			return;
		}

		onQuestionUpdate(editQuestion);
		setEditQuestion(null);
	};

	const onQuestionTextChange = (event, data) => {
		setEditQuestion((prevQuestion) => ({
			...prevQuestion,
			text: data.value,
		}));
	};

	const onDeleteClicked = () => {
		if (!user.admin && user.id !== question.author.id) {
			toast.warning("You can not delete somebody's else question");
			return;
		}

		setConfirmMsg("Are you sure you want to delete the question?");
	};

	const onDelete = async () => {
		setIsLoading(true);

		if (answers.length > 0) {
			const deleteAnswers = answers.map((answer) =>
				AnswerService.deleteAnswer(answer.id)
			);

			try {
				await Promise.all(deleteAnswers);
			} catch {
				toast.error("Something went wrong while deleting the answers");
			}
		}

		QuestionService.deleteQuestion(question.id)
			.then(() => {
				setConfirmMsg("");

				history.push("/questions");
			})
			.catch((error) => {
				toast.error("Something went wrong while deleting the question");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const updateScore = (addValue) => {
		onQuestionUpdate({
			...question,
			upvotes: question.upvotes + addValue,
		});
	};

	const onMinusClicked = () => updateScore(-1);
	const onPlusClicked = () => updateScore(1);

	return (
		<Grid centered columns={2}>
			{isLoading && (
				<Dimmer active>
					<Loader />
				</Dimmer>
			)}
			<Grid.Column>
				<Header as="h1" textAlign="center">
					{question.title}
				</Header>
				<Header textAlign="center" as="h5">
					Author: <strong>{question.author.username}</strong>
					<Label circular color="orange">{question.author.score}</Label>
				</Header>
				<Grid.Row>
					<Header textAlign="right" as="h5">
						Created on: <strong>{question.date}</strong>
					</Header>
				</Grid.Row>
				<Form>
					<TextArea
						value={editQuestion ? editQuestion.text : question.text}
						readOnly={editQuestion === null}
						style={{ minHeight: 200, marginBottom: 20 }}
						onChange={onQuestionTextChange}
					/>
				</Form>

				<Grid.Row style={{ marginBottom: 20 }}>
					{question.tagsList.map((tag) => (
						<Label as="a" key={tag.id} tag>
							{tag.name}
						</Label>
					))}
				</Grid.Row>

				<Button.Group floated="right" size="small">
					<Button
						color={editQuestion ? "orange" : "gray"}
						onClick={onEditClicked}
					>
						Edit
					</Button>
					<Button color="black" onClick={onDeleteClicked}>
						Delete
					</Button>
				</Button.Group>
				<Button.Group>
					<Button color="black" onClick={onMinusClicked}>
						<Icon name="minus" color="white" />
					</Button>
					<Button.Or text={question.upvotes} />
					<Button color="orange" onClick={onPlusClicked}>
						<Icon name="plus" color="black" />
					</Button>
				</Button.Group>

				<CommentsSection
					answers={sortByVotes(answers)}
					onAnswerSave={onAnswerSave}
					question={question}
					onAnswerDelete={onAnswerDelete}
					onAnswerUpdate={onAnswerUpdate}
				/>
			</Grid.Column>
			<Modal open={confirmMsg.length !== 0}>
				<Modal.Header>Delete</Modal.Header>
				<Modal.Content>{confirmMsg}</Modal.Content>
				<Modal.Actions>
					<Button onClick={onDelete} color="orange">
						Yes
					</Button>
					<Button onClick={() => setConfirmMsg("")} color="black">
						No
					</Button>
				</Modal.Actions>
			</Modal>
			<ToastContainer />
		</Grid>
	);
};

export default QuestionPage;
