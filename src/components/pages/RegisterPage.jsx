import React, { useState } from "react";
import { Link } from "react-router-dom";
import {
	Grid,
	Header,
	Form,
	Segment,
	Button,
	Message,
} from "semantic-ui-react";
import UserService from "../../services/UserService";
import { toast, ToastContainer } from "react-toastify";

const emptyUser = {
	username: "",
	password: "",
	confirmPassword: "",
	isAdmin: false,
	score: 0,
	isBanned: false,
	email: ""
};

const RegisterPage = (props) => {
	const [registerUser, setRegisterUser] = useState(emptyUser);

	const onUsernameChange = (event) => {
		setRegisterUser({ ...registerUser, username: event.target.value });
	};

	const onPasswordChange = (event) => {
		setRegisterUser({ ...registerUser, password: event.target.value });
	};

	const onEmailChange = (event) => {
		setRegisterUser({ ...registerUser, email: event.target.value });
	}

	const onConfirmPasswordChange = (event) => {
		setRegisterUser({
			...registerUser,
			confirmPassword: event.target.value,
		});
	};

	const toastError = (text) => {
		toast.error(text, {
			position: "top-right",
			autoClose: 5000,
			hideProgressBar: false,
			closeOnClick: true,
			pauseOnHover: true,
			draggable: true,
			progress: undefined,
		});
	};

	const onSubmitHandler = () => {
		if (registerUser.confirmPassword !== registerUser.password) {
			toastError("Passwords do not match");
			return;
		}
// eslint-disable-next-line
		if (!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(registerUser.email))
		{
			toastError("Invalid email, retry");
			return;
		}

		delete registerUser.confirmPassword;

		UserService.saveUser(registerUser)
			.then(() => {
				setRegisterUser(emptyUser);
				toast.success("User registered successfully");
			})
			.catch((err) => {
				toastError("Something went wrong while registering the user");
			});
	};

	return (
		<Grid
			textAlign="center"
			style={{ height: "100vh" }}
			verticalAlign="middle"
		>
			<Grid.Column style={{ maxWidth: 450 }}>
				<Header as="h2" color="orange" textAlign="center">
					Sign up
				</Header>
				<Form size="large">
					<Segment stacked>
						<Form.Input
							fluid
							icon="user"
							iconPosition="left"
							placeholder="Username"
							onChange={onUsernameChange}
						/>
						<Form.Input
							fluid
							icon="mail"
							iconPosition="left"
							placeholder="Email"
							onChange={onEmailChange}
						/>
						<Form.Input
							fluid
							icon="lock"
							iconPosition="left"
							placeholder="Password"
							type="password"
							onChange={onPasswordChange}
						/>
						<Form.Input
							fluid
							icon="lock"
							iconPosition="left"
							placeholder="Confirm Password"
							type="password"
							onChange={onConfirmPasswordChange}
						/>
						<Button
							color="orange"
							fluid
							size="large"
							onClick={onSubmitHandler}
							disabled={
								!(
									registerUser.password.trim().length > 0 &&
									registerUser.confirmPassword.trim().length > 0 &&
									registerUser.username.trim().length > 0 &&
									registerUser.email.trim().length > 0
								)
							}
						>
							Register
						</Button>
					</Segment>
				</Form>
				<Message>
					<Link to="/login">Go back to Login page</Link>
				</Message>
				<ToastContainer />
			</Grid.Column>
		</Grid>
	);
};

export default RegisterPage;
