import React, { useEffect, useState } from "react";
import {
	Label,
	Dimmer,
	Loader,
	Grid,
	Header,
	Button,
	Input,
	Icon,
} from "semantic-ui-react";
import TagService from "../../services/TagService";
import { toast } from "react-toastify";

const TagsPage = () => {
	const [tags, setTags] = useState([]);
	const [initialTags, setInitialTags] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	const [newTagName, setNewTagName] = useState("");

	useEffect(() => {
		setIsLoading(true);

		TagService.getAll()
			.then((response) => {
				setTags(response.data);
				setInitialTags(response.data);
			})
			.catch((err) => {
				toast.error("Something went wrong while getting the tags");
			})
			.finally(() => {
				setIsLoading(false);
			});
	}, []);

	const onTagSave = () => {
		setIsLoading(true);

		TagService.saveTag({ name: newTagName })
			.then((response) => {
				setTags([...tags, response.data]);
				setInitialTags([...initialTags, response.data]);
				toast.success("Tag created successfully");
				setNewTagName("");
			})
			.catch((err) => {
				toast.error("Something went wrong while saving the tag");
			})
			.finally(() => {
				setIsLoading(false);
			});
	};

	const onChange = (event, data) => {
		const value = data.value.toLowerCase();

		if (value.trim().length === 0) {
			setTags(initialTags);
			return;
		}

		const filteredTags = initialTags.filter((tag) =>
			tag.name.toLowerCase().includes(value)
		);

		setTags(filteredTags);
	};

	return (
		<>
			{isLoading && (
				<Dimmer active>
					<Loader />
				</Dimmer>
			)}
			<Grid>
				<Grid.Row>
					<Header
						as="h1"
						style={{
							margin: "0 auto",
							marginBottom: 20,
							marginTop: 20,
						}}
					>
						All Tags
					</Header>
				</Grid.Row>
			</Grid>
			<Grid
				style={{ margin: "0 auto", width: 1000, padding: 20 }}
				centered
			>
				<Grid.Row>
					<Input
						icon
						placeholder="Search..."
						onChange={onChange}
						style={{ width: "32%" }}
					>
						<input />
						<Icon name="search" />
					</Input>
				</Grid.Row>
				<Grid.Row>
					<Input
						placeholder="Tag name..."
						value={newTagName}
						onChange={(event, data) => setNewTagName(data.value)}
					/>
					<Button
						content="Add Tag"
						color="orange"
						onClick={onTagSave}
					/>
				</Grid.Row>
				<Grid.Row>
					{tags.map((tag) => (
						<Label as="a" style={{ margin: 25 }}>
							<Icon name="tag" size="big" />
							{tag.name}
						</Label>
					))}
				</Grid.Row>
			</Grid>
		</>
	);
};

export default TagsPage;
