import axios from 'axios'
import { ANSWER_URL } from './UrlMapping';

class AnswerService {
    getAll() {
        return axios.get(`${ANSWER_URL}`);
    }

    getAnswersByQuestion(questionId) {
        return axios.get(`${ANSWER_URL}/byQuestion/${questionId}`);
    }

    getAnswersByUser(userId) {
        return axios.get(`${ANSWER_URL}/byUser/${userId}`);
    }

    getAnswer(id) {
        return axios.get(`${ANSWER_URL}/${id}`);
    }

    deleteAnswer(id){
        return axios.delete(`${ANSWER_URL}/${id}`);
    }

    saveAnswer(answer) {
        return axios.post(`${ANSWER_URL}`, answer);
    }

    updateAnswer(answer) {
        return axios.put(`${ANSWER_URL}`, answer);
    }
}

export default new AnswerService();