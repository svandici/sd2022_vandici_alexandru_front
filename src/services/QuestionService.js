import axios from 'axios'
import { QUESTION_URL } from './UrlMapping';

class QuestionService {
    getAll() {
        return axios.get(`${QUESTION_URL}`)
    }

    getQuestionsByUser(userId) {
        return axios.get(`${QUESTION_URL}/byUser/${userId}`)
    }

    getQuestion(id) {
        return axios.get(`${QUESTION_URL}/${id}`);
    }

    deleteQuestion(id){
        return axios.delete(`${QUESTION_URL}/${id}`);
    }

    saveQuestion(question) {
        return axios.post(`${QUESTION_URL}`, question);
    }

    updateQuestion(question) {
        return axios.put(`${QUESTION_URL}`, question);
    }
}

export default new QuestionService();