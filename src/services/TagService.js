import axios from 'axios'
import { TAG_URL } from './UrlMapping';

class TagService {
    getAll() {
        return axios.get(`${TAG_URL}`)
    }

    getTag(id) {
        return axios.get(`${TAG_URL}/${id}`);
    }

    deleteTag(id){
        return axios.delete(`${TAG_URL}/${id}`);
    }

    saveTag(tag) {
        return axios.post(`${TAG_URL}`, tag);
    }

    updateTag(tag) {
        return axios.put(`${TAG_URL}`, tag);
    }
}

export default new TagService();