const API = 'http://localhost:8080'

export const USER_URL = `${API}/user`;
export const TAG_URL = `${API}/tag`;
export const QUESTION_URL = `${API}/question`;
export const ANSWER_URL = `${API}/answer`;