import axios from 'axios'
import { USER_URL } from './UrlMapping';

class UserService {
    login(loginRequest) {
        return axios.post(`${USER_URL}/login`, loginRequest);
    }

    getAll() {
        return axios.get(`${USER_URL}`)
    }

    getUser(id) {
        return axios.get(`${USER_URL}/${id}`);
    }

    deleteUser(id){
        return axios.delete(`${USER_URL}/${id}`);
    }

    saveUser(user) {
        return axios.post(`${USER_URL}`, user);
    }

    updateUser(user) {
        return axios.put(`${USER_URL}`, user);
    }
}

export default new UserService();